// Copyright (c) 2017, Boris Bogaerts
// All rights reserved.

// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:

// 1. Redistributions of source code must retain the above copyright 
// notice, this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its 
// contributors may be used to endorse or promote products derived from 
// this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "vrep_scene_content.h"
#include "vrep_mesh_reader.h"
#include <vector>
#include "extApi.h"
#include <chrono>
#include <iostream>
#include <string>
#include <sstream>
#include <iterator>
#include <fstream>
#include <vtkOpenVRModel.h>

#include <vtkOpenVRInteractorStyle.h>
#include "vrep_controlled_object.h"

typedef std::chrono::high_resolution_clock Clock;

// Everything to catch controller events
class eventCatcher : public vtkOpenVRInteractorStyle
{
public:
	void OnButton3D(vtkEventData *edata) {
		int mode;
		bool trackpad = false;
		std::string signalName;
		vtkEventDataDeviceInput button = edata->GetAsEventDataDevice3D()->GetInput();
		vtkEventDataDevice controller = edata->GetAsEventDataDevice3D()->GetDevice();
		vtkEventDataAction action = edata->GetAsEventDataDevice3D()->GetAction();
		if (controller == vtkEventDataDevice::LeftController) {
			signalName = "LeftController";
		}
		else if (controller == vtkEventDataDevice::RightController) {
			signalName = "RightController";
		}

		if (button == vtkEventDataDeviceInput::Trigger) {
			signalName.append("Trigger");
		}
		else if (button == vtkEventDataDeviceInput::Grip) {
			signalName.append("Grip");
		}
		else if (button == vtkEventDataDeviceInput::TrackPad) {
			trackpad = true;
			signalName.append("TrackPad");
		}

		if (action == vtkEventDataAction::Press) {
			mode = 1;
		}
		else if (action == vtkEventDataAction::Release) {
			mode = 0;
		}

		if (trackpad) {
			std::string signalName2;
			signalName2 = signalName;
			signalName.append("X");
			signalName2.append("Y");
			vtkOpenVRRenderWindowInteractor* rw = vtkOpenVRRenderWindowInteractor::SafeDownCast(this->Interactor); // get renderwindowinteractor
			simxSetFloatSignal(clientID, signalName.c_str(), mode * rw->GetTouchPadPosition()[0], simx_opmode_oneshot); // get last touchpad position
			simxSetFloatSignal(clientID, signalName2.c_str(), mode * rw->GetTouchPadPosition()[1], simx_opmode_oneshot);
		}
		else {
			simxSetIntegerSignal(clientID, signalName.c_str(), mode, simx_opmode_oneshot);
		}
	}
	int clientID;
protected:
};

// frame rate class
class framerate_counter {
public:
	void set_interval(int interval) { countInterval = interval; std::cout  << "fps: 0"; };
	void increment();
private:
	int counter = 0;
	int countInterval = 10;
	std::chrono::steady_clock::time_point t1;
};

void framerate_counter::increment() {
		if (counter == 0) {
			t1 = Clock::now();
		}; counter++;

		if (counter == countInterval) {
			counter = 0;
			std::chrono::steady_clock::time_point t2 = Clock::now();
			double time = (std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count());
			time = (1000 * countInterval) / (time);
			std::cout << '\r' <<"fps: " << time;
		}
}

// pose conversion function 

vtkSmartPointer<vtkTransform> getTransform(int deviceIdx, vtkSmartPointer<vtkOpenVRRenderWindow> rw, vtkSmartPointer<vtkOpenVRRenderWindowInteractor> rwi);

// vrep_scene_content //

vrep_scene_content::~vrep_scene_content() {
}


void vrep_scene_content::loadScene() {
	int counter = 0;
	int v = 10;
	int h = 0;
	int clientID = 0;
	// interpret extra instructions
	vrepMeshContainer.reserve(200); // I do not know why, but this is necessary
	// read geometry
	while (v > 0) { 
		vrep_mesh_reader temp;
		vrep_mesh_object temp2;
		counter = temp.read_mesh(clientID, counter, h, v);
		if (v > 0) {
			// Get data for vr 				
			vrepMeshContainer.push_back(temp2);
			vrepMeshContainer[vrepMeshContainer.size()-1].extractDataFromReader(temp);
			// General things
			vrepMeshContainer[vrepMeshContainer.size() - 1].setClientID(clientID, refHandle);
			vrepMeshContainer[vrepMeshContainer.size() - 1].setHandle(h);
			vrepMeshContainer[vrepMeshContainer.size() - 1].makeActor();
		}
	}
	cout << "Number of objects loaded : " << vrepMeshContainer.size() << endl;		
}

void vrep_scene_content::vrep_get_object_pose() {
	for (int i = 0; i < vrepMeshContainer.size(); i++) {
		vrepMeshContainer[i].updatePosition();
	}
}


void vrep_scene_content::activate_interactor() {
	// add actors
	for (int i = 0; i < vrepMeshContainer.size(); i++) {
		renderer->AddActor(vrepMeshContainer[i].getActor());
	}
	renderer->SetBackground(1.0, 1.0, 1.0);
	renderer->SetActiveCamera(vr_camera);
	//renderer->SetShowFloor(true);
	//vtkSmartPointer<vtkLight> light = vtkSmartPointer<vtkLight>::New();
	//light->SetFocalPoint(1.875, 0.6125, 0);
	//light->SetPosition(0.875, 1.6125, 1);
	//renderer->AddLight(light);
	renderWindow->AddRenderer(renderer);
	renderWindow->SetDesiredUpdateRate(150);
	vr_renderWindowInteractor->SetRenderWindow(renderWindow);

	this->vrep_get_object_pose();


	vrep_controlled_object *controller1_vrep = new vrep_controlled_object(clientID, refHandle);
	vrep_controlled_object *controller2_vrep = new vrep_controlled_object(clientID, refHandle);
	vrep_controlled_object *basestation1_vrep = new vrep_controlled_object(clientID, refHandle);
	vrep_controlled_object *basestation2_vrep = new vrep_controlled_object(clientID, refHandle);
	vrep_controlled_object *headset_vrep = new vrep_controlled_object(clientID, refHandle);

		//// Get controller models
	int vtkDevices[4] = {-1, -1,-1, -1};
		int controllerCounter = 0;
		int basestationCounter = 0;
		std::string controllername = "vr_controller_vive_1_5";
		std::string basestationname = "lh_basestation_vive";
		renderWindow->Render(); // necessary, otherwise devices not detected
		headset_vrep->setName("Headset");
		for (int i = 0; i < 6; i++) { // Detect controllers and basestations 
			if (renderWindow->GetTrackedDeviceModel(i) != nullptr) {
				cout << "Device with name : " << renderWindow->GetTrackedDeviceModel(i)->GetName() << " discovered." << endl;
				if (controllername.compare(renderWindow->GetTrackedDeviceModel(i)->GetName())==0 && controllerCounter ==0) {
					cout << "    Controller 1 recognized" << endl;
					vtkDevices[0] = i;
					controller1_vrep->setName("Controller1");
					controllerCounter++;
				}else if (controllername.compare(renderWindow->GetTrackedDeviceModel(i)->GetName())==0 && controllerCounter ==1) {
						cout << "    Controller 2 recognized" << endl;
						vtkDevices[1] = i;
						controller2_vrep->setName("Controller2");
						controllerCounter++;
					}
				else if (basestationname.compare(renderWindow->GetTrackedDeviceModel(i)->GetName()) == 0 && basestationCounter == 0) {
					cout << "    Base station 1 recognized" << endl;
					vtkDevices[2] = i;
					basestation1_vrep->setName("BaseStation1");
					basestationCounter++;
				}
				else if (basestationname.compare(renderWindow->GetTrackedDeviceModel(i)->GetName()) == 0 && basestationCounter == 1) {
					cout << "    Base station 2 recognized" << endl;
					vtkDevices[3] = i;
					basestation2_vrep->setName("BaseStation2");
					basestationCounter++;
				}
			}else {
				cout << "Device with id number " << i << " not found" << endl;
			}
		}

		eventCatcher *events = new(eventCatcher); // define object that captures buttonpress
		events->clientID = clientID;
		framerate_counter fps; // framerate counter object
		vr_renderWindowInteractor->SetInteractorStyle(events); // set object which captures buttonpress

		while (true) {
			this->vrep_get_object_pose(); // update the v-rep mesh in vtk
			if (controllerCounter > 0) { controller1_vrep->updatePosition(getTransform(vtkDevices[0], renderWindow, vr_renderWindowInteractor)); }; // update pose vrep dummy
			if (controllerCounter > 1) { controller2_vrep->updatePosition(getTransform(vtkDevices[1], renderWindow, vr_renderWindowInteractor)); };// update pose vrep dummy
			if (basestationCounter > 0) { basestation1_vrep->updatePosition(getTransform(vtkDevices[2], renderWindow, vr_renderWindowInteractor)); };// update pose vrep dummy
			if (basestationCounter > 1) { basestation2_vrep->updatePosition(getTransform(vtkDevices[3] , renderWindow, vr_renderWindowInteractor)); };// update pose vrep dummy
			
			headset_vrep->updatePosition(getTransform(-1, renderWindow, vr_renderWindowInteractor)); // update pose vrep dummy

			vr_renderWindowInteractor->DoOneEvent(renderWindow, renderer); // render
			fps.increment(); // fps recorder

			if (simxGetLastCmdTime(clientID) <= 0) {
				vr_renderWindowInteractor->TerminateApp(); // stop if the v-rep simulation is not running
				return;
			}
		}
}


vtkSmartPointer<vtkTransform> getTransform(int deviceIdx, vtkSmartPointer<vtkOpenVRRenderWindow> rw ,vtkSmartPointer<vtkOpenVRRenderWindowInteractor> rwi) {
	vtkSmartPointer<vtkTransform> tform = vtkSmartPointer<vtkTransform>::New();
	double pos[3];
	double wxyz[4];
	double ppos[3];
	double wdir[3];
	tform->PostMultiply();
	if (deviceIdx == -1) {
		vr::TrackedDevicePose_t& vrPose = rw->GetTrackedDevicePose(vtkEventDataDevice::HeadMountedDisplay); // this is actually the right way to do this, but there is no option for base stations. On the other hand, with integers is only possible for controllers and basestation
		rwi->ConvertPoseToWorldCoordinates(vrPose, pos, wxyz, ppos, wdir);
	}
	else {
		vr::TrackedDevicePose_t& vrPose = rw->GetTrackedDevicePose(deviceIdx);
		rwi->ConvertPoseToWorldCoordinates(vrPose, pos, wxyz, ppos, wdir);
	}
	
	tform->RotateWXYZ(wxyz[0], wxyz[1], wxyz[2], wxyz[3]);
	tform->Translate(pos);
	return tform;
}