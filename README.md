# V-REP HTC Vive interface #

This repository contains code to use HTC-Vive virtual reality device in the V-REP robot simulation platform. The project generates an executionable standalone application. This application tries to connect with a running V-REP simulation. If the application detects the V-REP simulation, it transfers the geometry to a VTK openvr renderwindow. The poses of objects are continuously updated. The pose of the controllers are updated in V-REP. 

Current functionalities:

- Pose of controllers/basestations/head mounted display are sent to dummies in V-REP

- Button press events are sent to V-REP integer/float signal (touchpad/trigger/handle)

- V-REP geometry is transfered and object positions are synchronized

## Dependencies ##
- VREP 3.4.0 (rev 1)
http://www.coppeliarobotics.com/downloads.html
- VTK 8.1.0 (virtual reality devices are in development and changing a lot, this version is strictly necessary)
https://github.com/Kitware/VTK
- openvr SDK
https://github.com/ValveSoftware/openvr
- EIGEN3
https://eigen.tuxfamily.org

## How do I get set up? ##

### Getting .exe file ###

It is highly recommended to just use the binaries compiled on a windows 64 bit machine using visual studio 2015. All necessary .dll files are also included.

Currently the provided microsoft visual studio solution file is not general. All included directories and libraries are hard coded and should be replaced by correct folders in order to succesfully build the project.

### Running the application ###

- Set up HTC Vive glasses

- Run steam/steamVR

- Launch example scene (or scene with HTC_VIVE.ttm object in)

- Launch vrepVIVE.exe

# License #

The software is licensed under the BSD License (see LICENSE.txt) and uses third party libraries that are distributed under their own terms (see LICENSE-3RD-PARTY.txt).